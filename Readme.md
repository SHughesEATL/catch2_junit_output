## Build

```
$ mkdir build
$ cd build
$ cmake ..
$ cmake --build .
```

## Running tests

### Passes

Run the test:
```
$ ctest
```

Output:

```
Test project /home/sion/wsl_git/catch2_junit_out_bug/build
    Start 1: Check all Values
1/1 Test #1: Check all Values .................   Passed  690.23 sec

100% tests passed, 0 tests failed out of 1

Total Test time (real) = 690.23 sec
```

### Fails

Run the test:
```
$ ./tests/tests -r junit -o tests.xml
```

Output:
```
terminate called after throwing an instance of 'std::bad_alloc'
  what():  std::bad_alloc
terminate called recursively
[1]    4521 abort      ./tests/tests -r junit -o tests.xml
```