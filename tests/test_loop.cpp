#include <catch2/catch.hpp>

#include "../loop.h"

TEST_CASE("Check all Values")
{
    Loop loop;

    for(uint32_t counter_{0}; counter_ < std::numeric_limits<uint32_t>::max(); ++counter_)
    {
        REQUIRE(loop.GetValue() == counter_);
        loop.Increment();
    }

}

