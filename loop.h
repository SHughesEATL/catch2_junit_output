#ifndef LOOP_H
#define LOOP_H

#include <cstdint>

class Loop
{
  public:
    Loop() = default;
    ~Loop() = default;
    void Increment();
    uint32_t GetValue();

  private:
    uint32_t counter_value_{0};
};

#endif //LOOP_H