#include "loop.h"

void Loop::Increment()
{
    ++counter_value_;
}

uint32_t Loop::GetValue()
{
    return counter_value_;
}